package com.moises.filminfo.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks
import com.github.ksoichiro.android.observablescrollview.ScrollState
import com.moises.filminfo.R
import com.moises.filminfo.model.Movie
import com.nineoldandroids.view.ViewHelper
import java.util.*


class MovieViewPagerAnimator(var hvp: MoviesViewPager) : OnPageChangeListener {
    var initialHeaderHeight = -1f
    var finalHeaderHeight = -1f
    var scrolls: MutableList<Any> = ArrayList()
    var calledScrolls: MutableList<Any> = ArrayList()
    var oldpage = -2
    protected fun onPageScroll(position: Int) {
        if (position != oldpage) {
            oldpage = position
            var i = 0
            val size = hvp.headerHolders.size
            while (i < size) {
                if (i == position) {
                    hvp.headerHolders[i]!!.animateEnabled(true)
                    if (!hvp.headerHolders[i]!!.isVisible) {
                        if (i - 1 > 0) hvp.headerScroll!!.smoothScrollTo(
                            hvp.headerHolders[i - 1]!!.view.left,
                            0
                        ) else hvp.headerScroll!!.smoothScrollTo(
                            hvp.headerHolders[i]!!.view.left, 0
                        )
                    }
                } else {
                    hvp.headerHolders[i]!!.animateEnabled(false)
                }
                ++i
            }
        }
    }

    fun onScroll(source: Any, verticalOffset: Int) {
        dispatchScroll(source, verticalOffset)
        hvp.headerScroll!!.translationY = -verticalOffset.toFloat()
        if (hvp.headerScroll!!.translationY > 0) hvp.headerScroll!!.translationY = 0f
        if (initialHeaderHeight <= 0) {
            initialHeaderHeight = hvp.headerScroll!!.height.toFloat()
            finalHeaderHeight = initialHeaderHeight / 2
        }
        if (initialHeaderHeight > 0) {
            if (verticalOffset < finalHeaderHeight) {
                val percent = (initialHeaderHeight - verticalOffset) / initialHeaderHeight
                val page = Math.max(0, oldpage)

                //headerLayout.setPivotY(0);
                ViewHelper.setPivotX(
                    hvp.headerLayout,
                    hvp.headerLayout!!.getChildAt(page).left.toFloat()
                )
                ViewHelper.setScaleX(hvp.headerLayout, percent)
                ViewHelper.setScaleY(hvp.headerLayout, percent)
                ViewHelper.setTranslationY(hvp.headerLayout, (verticalOffset / 2).toFloat())
                ViewHelper.setTranslationX(hvp.headerLayout, 1.5f * verticalOffset)
                val alphaPercent = 1 - verticalOffset / finalHeaderHeight
                var i = 0
                val count = hvp.headerHolders.size
                while (i < count) {
                    val headerHolder = hvp.headerHolders[i]
                    headerHolder!!.textView.alpha = alphaPercent
                    headerHolder.view.translationX =
                        -i * 4 * headerHolder.view.paddingLeft * (1 - percent)
                    ++i
                }
            }
        }
    }

    fun fillHeader(adapter: PagerAdapter, movies: List<Movie>) {
        hvp.headerLayout!!.removeAllViews()
        val layoutInflater = LayoutInflater.from(hvp.context)
        for (i in 0 until adapter.count) {
            val view = layoutInflater.inflate(R.layout.header_card, hvp.headerLayout, false)
            hvp.headerLayout!!.addView(view)
            val headerHolder = HeaderHolder(view)
            hvp.headerHolders.add(headerHolder)
            headerHolder.setTitle(adapter.getPageTitle(i))
            headerHolder.setImageCover("https://image.tmdb.org/t/p/w185/"+ movies[i].posterPath)
            hvp.configurator?.let { headerHolder.setHeightPercent(it.getHeightPercentForPage(i)) }
            headerHolder.isEnabled = i == 0
            view.setOnClickListener { onHeaderClick(i) }
        }
    }

    fun onHeaderClick(position: Int) {
        hvp.viewPager!!.setCurrentItem(position, true)
    }

    fun registerRecyclerView(recyclerView: RecyclerView) {
        scrolls.add(recyclerView)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (calledScrolls.contains(recyclerView)) calledScrolls.remove(recyclerView) else {
                    onScroll(recyclerView, recyclerView.computeVerticalScrollOffset())
                }
            }
        })
    }

    fun registerScrollView(scrollView: ObservableScrollView) {
        scrolls.add(scrollView)
        if (scrollView.parent != null && scrollView.parent.parent != null && scrollView.parent.parent is ViewGroup) scrollView.setTouchInterceptionViewGroup(
            scrollView.parent.parent as ViewGroup
        )
        scrollView.setScrollViewCallbacks(object : ObservableScrollViewCallbacks {
            override fun onScrollChanged(i: Int, b: Boolean, b1: Boolean) {
                onScroll(scrollView, i)
            }

            override fun onDownMotionEvent() {}
            override fun onUpOrCancelMotionEvent(scrollState: ScrollState) {}
        })
    }

    fun dispatchScroll(source: Any, yOffset: Int) {
        for (scroll in scrolls) {
            if (scroll !== source) {
                calledScrolls.add(scroll)
                if (scroll is RecyclerView) {
                    val recyclerView = scroll
                    if (recyclerView != null && recyclerView.layoutManager != null && recyclerView.layoutManager is LinearLayoutManager) {
                        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                        linearLayoutManager!!.scrollToPositionWithOffset(0, -yOffset)
                    }
                } else if (scroll is ScrollView) {
                    scroll.scrollTo(0, yOffset)
                }
            }
        }
        calledScrolls.clear()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {
        onPageScroll(position)
    }

    override fun onPageScrollStateChanged(state: Int) {}

    init {
        hvp.viewPager!!.addOnPageChangeListener(this)
    }
}