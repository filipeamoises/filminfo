package com.moises.filminfo.adapters

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.moises.filminfo.R
import com.moises.filminfo.model.Movie
import java.util.*


class MoviesViewPager : FrameLayout {
    protected var animator: MovieViewPagerAnimator? = null
    var settings = MovieViewPagerSettings()
    var headerScroll: HorizontalScrollView? = null
    var headerLayout: ViewGroup? = null
    var viewPager: ViewPager? = null
    var configurator: MovieViewPagerConfigurator? = null
    var headerHolders: MutableList<HeaderHolder?> = ArrayList()

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        settings.handleAttributes(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        settings.handleAttributes(context, attrs)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        MovieViewPagerBus.register(context, this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        MovieViewPagerBus.unregister(context)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        addView(LayoutInflater.from(context).inflate(R.layout.movies_view_pager, this, false))
        viewPager = findViewById<View>(R.id.bfp_viewPager) as ViewPager
        headerScroll = findViewById<View>(R.id.bfp_headerScroll) as HorizontalScrollView
        headerLayout = findViewById<View>(R.id.bfp_headerLayout) as ViewGroup
        run {
            val layoutParams = headerLayout!!.layoutParams
            layoutParams.height = this.settings.headerHeightPx
            headerLayout!!.layoutParams = layoutParams
        }
        animator = MovieViewPagerAnimator(this)
    }

    fun setAdapter(adapter: PagerAdapter, movies: List<Movie>) {
        viewPager!!.adapter = adapter
        animator!!.fillHeader(adapter, movies)
        viewPager!!.offscreenPageLimit = adapter.count //TODO remove
    }

    fun registerRecyclerView(recyclerView: RecyclerView) {
        animator!!.registerRecyclerView(recyclerView)
    }

    fun registerScrollView(scrollView: ObservableScrollView) {
        animator!!.registerScrollView(scrollView)
    }
}