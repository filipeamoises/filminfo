package com.moises.filminfo.adapters

/**
 * Created by florentchampigny on 10/08/15.
 */
interface MovieViewPagerConfigurator {
    fun getHeightPercentForPage(page: Int): Float
}