package com.moises.filminfo.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.moises.filminfo.application.bases.BaseFragment

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(
    fragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {

    private val fragments: ArrayList<BaseFragment> = arrayListOf()
    private val titles: ArrayList<String> = arrayListOf()

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun addTitles(titles: Array<String>) {
        this.titles.clear()
        this.titles.addAll(titles)
        notifyDataSetChanged()
    }

    fun addFragments(fragments: ArrayList<BaseFragment>) {
        this.fragments.clear()
        this.fragments.addAll(fragments)
        notifyDataSetChanged()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        if (titles.isEmpty()) {
            return null
        }
        return titles[position]
    }

    fun clearFragments() {
        this.fragments.clear()
        notifyDataSetChanged()
    }

    fun addFragment(fragment: BaseFragment) {
        this.fragments.add(fragment)
        notifyDataSetChanged()
    }
}