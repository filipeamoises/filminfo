package com.moises.filminfo.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import java.util.*


object MovieViewPagerBus {
    var map: MutableMap<Context, MoviesViewPager> = HashMap()
    fun register(context: Context, moviesViewPager: MoviesViewPager) {
        map[context] = moviesViewPager
    }

    fun unregister(context: Context) {
        map.remove(context)
    }

    fun registerScrollView(context: Context, scrollView: ObservableScrollView) {
        val hollyViewPager = map[context]
        hollyViewPager?.registerScrollView(scrollView)
    }

    fun registerRecyclerView(context: Context, recyclerView: RecyclerView) {
        val hollyViewPager = map[context]
        hollyViewPager?.registerRecyclerView(recyclerView)
    }

    operator fun get(context: Context): MoviesViewPager? {
        return map[context]
    }
}