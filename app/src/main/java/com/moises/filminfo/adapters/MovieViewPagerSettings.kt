package com.moises.filminfo.adapters

import android.content.Context
import android.util.AttributeSet
import com.moises.filminfo.R

/**
 * Created by florentchampigny on 10/08/15.
 */
class MovieViewPagerSettings {
    var headerHeightPx = 0
    protected var headerHeight = 0
    fun handleAttributes(context: Context, attrs: AttributeSet?) {
        try {
            val styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.HollyViewPager)
            run {
                headerHeightPx = styledAttrs.getDimensionPixelOffset(
                    R.styleable.HollyViewPager_hvp_headerHeight,
                    -1
                )
                if (headerHeightPx == -1) {
                    headerHeightPx = context.resources.getDimensionPixelOffset(R.dimen.dimen_150dp)
                }
                headerHeight = Math.round(pxToDp(headerHeightPx.toFloat(), context)) //convert to dp
            }
            styledAttrs.recycle()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        fun pxToDp(px: Float, context: Context): Float {
            return px / context.resources.displayMetrics.density
        }
    }
}