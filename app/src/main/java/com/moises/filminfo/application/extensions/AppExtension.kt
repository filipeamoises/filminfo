package com.moises.filminfo.application.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.moises.filminfo.application.FilminfoApplication
import com.moises.filminfo.application.helpers.ContextHelper

fun Any.getAppContext(): Context {
    return ContextHelper.getAppContext()
}

fun Any.TAGAPP(): String {
    return FilminfoApplication::class.java.simpleName
}

fun Any.hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}
