package com.moises.filminfo.application.bases

import android.content.Intent
import androidx.fragment.app.Fragment
import com.moises.filminfo.R
import org.jetbrains.annotations.NotNull

open class BaseFragment : Fragment() {


    fun startActivityWithAnimation(@NotNull intent: Intent) {
        startActivity(intent)
        configureSlideEnterTransition()
    }

    fun startActivityForResultWithAnimation(@NotNull intent: Intent, @NotNull codeResult: Int) {
        startActivityForResult(intent, codeResult)
        configureSlideEnterTransition()
    }

    fun finishWithSlideAnimation() {
        requireActivity().finish()
        configureSlideExitTransition()
    }

    private fun configureSlideEnterTransition() {
        requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private fun configureSlideExitTransition() {
        requireActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}