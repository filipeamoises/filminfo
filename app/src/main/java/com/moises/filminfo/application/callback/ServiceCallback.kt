package com.moises.filminfo.application.callback

import androidx.lifecycle.MutableLiveData
import com.moises.filminfo.R
import com.moises.filminfo.application.helpers.ResourcesHelper
import com.moises.filminfo.model.ListMovieResponse
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.moises.filminfo.model.Response as ResponseFilminfo


class ServiceCallback<C> {
    fun build(ret: MutableLiveData<ResponseFilminfo<C>>): Callback<C> {


        return object : Callback<C> {

            override fun onResponse(call: Call<C>, response: Response<C>) {

                try {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            ret.postValue(ResponseFilminfo(it, null))
                        } ?: run {
                            onFailure(call, Throwable(ResourcesHelper.getAppString(R.string.error_generic_callback)))
                        }

                    } else {

                        val throwable: Throwable

                        throwable = if (response.errorBody() != null) {

                            val jObjError = JSONObject(response.errorBody()!!.string())

                            if (!jObjError.isNull("error")) {
                                Throwable(jObjError.get("error").toString())
                            } else if (!jObjError.isNull("message")) {
                                Throwable(jObjError.get("message").toString())
                            } else {
                                Throwable(ResourcesHelper.getAppString(R.string.error_generic_callback))
                            }

                        } else {
                            Throwable(response.message())
                        }
                        onFailure(call, throwable)
                    }

                } catch (e: Exception) {
                    onFailure(call, Throwable(e.message))
                }
            }


            override fun onFailure(call: Call<C>, t: Throwable) {
                ret.postValue(ResponseFilminfo(null, t, false))
            }
        }

    }

    fun build(viewModelCallback: ViewModelCallback<C>): Callback<C> {

        return object : Callback<C> {

            override fun onResponse(call: Call<C>, response: Response<C>) {

                try {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            viewModelCallback.onSuccess(it)
                        } ?: run {
                            onFailure(call, Throwable(ResourcesHelper.getAppString(R.string.error_generic_callback)))
                        }

                    } else {

                        val throwable: Throwable

                        throwable = if (response.errorBody() != null) {

                            val jObjError = JSONObject(response.errorBody()!!.string())

                            if (!jObjError.isNull("error")) {
                                Throwable(jObjError.get("error").toString())
                            } else if (!jObjError.isNull("message")) {
                                Throwable(jObjError.get("message").toString())
                            } else {
                                Throwable(ResourcesHelper.getAppString(R.string.error_generic_callback))
                            }

                        } else {
                            Throwable(response.message())
                        }
                        onFailure(call, throwable)
                    }

                } catch (e: Exception) {
                    onFailure(call, Throwable(ResourcesHelper.getAppString(R.string.error_generic_callback)))
                }
            }


            override fun onFailure(call: Call<C>, t: Throwable) {
                viewModelCallback.onError(t)
            }
        }
    }

}