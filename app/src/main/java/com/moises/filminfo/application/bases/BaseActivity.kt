package com.moises.filminfo.application.bases

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.moises.filminfo.R
import org.jetbrains.annotations.NotNull


open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    fun startActivityWithAnimation(@NotNull intent: Intent) {
        startActivity(intent)
        configureSlideEnterTransition()
    }

    fun startActivityForResultWithAnimation(@NotNull intent: Intent, @NotNull codeResult: Int) {
        startActivityForResult(intent, codeResult)
        configureSlideEnterTransition()
    }

    fun finishWithSlideAnimation() {
        super.finish()
        configureSlideExitTransition()
    }

    private fun configureSlideEnterTransition() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private fun configureSlideExitTransition() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}
