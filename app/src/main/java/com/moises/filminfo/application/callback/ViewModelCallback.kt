package com.moises.filminfo.application.callback

interface ViewModelCallback<C> {
    abstract fun onSuccess(response: C)
    abstract fun onError(throwable: Throwable)
}