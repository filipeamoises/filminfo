package com.moises.filminfo.application

import androidx.multidex.MultiDexApplication
import com.moises.filminfo.application.helpers.ContextHelper

class FilminfoApplication : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()

        ContextHelper.setAppContext(applicationContext)

    }
}