package com.moises.filminfo.application.helpers

import android.widget.Toast
import com.moises.filminfo.application.extensions.getAppContext

class ToastHelper {

    companion object {

        fun showShortToast(id: Int) {
            Toast.makeText(getAppContext(), id, Toast.LENGTH_SHORT).show()
        }

        fun showShortToast(text: String) {
            Toast.makeText(getAppContext(), text, Toast.LENGTH_SHORT).show()
        }

        fun showLongToast(id: Int) {
            Toast.makeText(getAppContext(), id, Toast.LENGTH_LONG).show()
        }

        fun showLongToast(text: String) {
            Toast.makeText(getAppContext(), text, Toast.LENGTH_LONG).show()
        }

    }
}