package com.moises.filminfo.application.helpers

import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.moises.filminfo.application.extensions.getAppContext

class ResourcesHelper {

    companion object {

        fun getAppString(@StringRes idRes: Int, vararg formatArgs: Any): String {
            return getAppContext().getString(idRes, formatArgs)
        }

        fun getAppDrawable(@DrawableRes idDra: Int): Drawable? {
            return getAppContext().getDrawable(idDra)
        }

        fun getAppColor(@ColorRes idColor: Int): Int {
            return ContextCompat.getColor(getAppContext(), idColor)
        }


    }
}