package com.moises.filminfo.application.exception

import com.moises.filminfo.R
import com.moises.filminfo.application.helpers.ResourcesHelper
import java.io.IOException

class InternetException : IOException() {

    override val message: String
        get() = ResourcesHelper.getAppString(R.string.offline)
}