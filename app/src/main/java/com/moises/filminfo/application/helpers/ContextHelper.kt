package com.moises.filminfo.application.helpers

import android.content.Context
import org.jetbrains.annotations.NotNull

class ContextHelper {

    companion object {

        private lateinit var contextApplication: Context

        fun getAppContext(): Context {
            return contextApplication
        }

        fun setAppContext(@NotNull context: Context) {
            contextApplication = context
        }

    }
}