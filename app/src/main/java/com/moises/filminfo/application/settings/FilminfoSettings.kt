package com.moises.filminfo.application.settings

import com.moises.filminfo.BuildConfig


class FilminfoSettings {

    companion object{

        const val baseURLAPI = BuildConfig.URL_API
        const val baseKEYAPI = BuildConfig.KEY_API

    }
}