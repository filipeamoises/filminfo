package com.moises.filminfo.application.bases

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
}