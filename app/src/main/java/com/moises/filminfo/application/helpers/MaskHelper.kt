package com.moises.filminfo.application.helpers

import android.app.Activity
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.moises.filminfo.application.extensions.hideKeyboard
import org.jetbrains.annotations.NotNull


class MaskHelper {


    companion object {

        const val FORMAT_CPF = "###.###.###-##"
        const val FORMAT_FONE = "(##)####-#####"
        const val FORMAT_CEP = "#####-###"
        const val FORMAT_DATE = "##/##/####"
        const val FORMAT_HOUR = "##:##"


        fun mask(@NotNull ediTxt: EditText, @NotNull mask: String, activity: Activity? = null): TextWatcher {

            var isUpdating = false
            var old = ""

            return object : TextWatcher {

                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    activity?.let { act ->
                        if (mask.length == count) {
                            hideKeyboard(act)
                        }
                    }

                    val str = unmask(s.toString())
                    var mascara = ""

                    if (isUpdating) {
                        old = str
                        isUpdating = false
                        return
                    }

                    var i = 0

                    for (m in mask.toCharArray()) {
                        if (m != '#' && str.length > old.length) {
                            mascara += m
                            continue
                        }
                        try {
                            mascara += str[i]
                        } catch (e: Throwable) {
                            break
                        }

                        i++
                    }
                    isUpdating = true
                    ediTxt.setText(mascara)
                    ediTxt.setSelection(mascara.length)

                }
            }
        }

        fun unmask(s: String): String {
            return s.replace(".", "").replace("-", "").replace("/", "").replace("(", "")
                .replace(" ", "").replace(":", "").replace(")", "")

        }

    }


}