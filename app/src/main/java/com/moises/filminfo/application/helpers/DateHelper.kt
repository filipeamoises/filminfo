package com.moises.filminfo.application.helpers

import java.text.SimpleDateFormat
import java.util.*


class DateHelper {

    companion object {

        fun getDateNow(): String {
            val c = Calendar.getInstance()
            val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            return sdf.format(c.time)

        }

    }
}