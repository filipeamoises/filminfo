package com.moises.filminfo.application.extensions

import android.text.TextUtils
import android.widget.EditText

fun EditText.emailIsValid(): Boolean {
    return if (TextUtils.isEmpty(this.text)) {
        false
    } else {
        android.util.Patterns.EMAIL_ADDRESS.matcher(this.text).matches()
    }
}
