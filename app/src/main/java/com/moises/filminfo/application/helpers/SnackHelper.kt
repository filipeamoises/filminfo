package com.moises.filminfo.application.helpers

import android.view.View

import com.google.android.material.snackbar.Snackbar
import org.jetbrains.annotations.NotNull

class SnackHelper {

    companion object {
        fun showSnack(@NotNull view: View, idString: Int) {
            Snackbar.make(view, idString, Snackbar.LENGTH_SHORT).show()
        }

        fun showSnack(@NotNull view: View, @NotNull message: String) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
        }

        fun showSnackWithAction(
            @NotNull view: View,
            idString: Int,
            idStringAction: Int,
            onclick: View.OnClickListener
        ) {
            Snackbar.make(view, idString, Snackbar.LENGTH_SHORT).setAction(idStringAction, onclick)
                .show()
        }

        fun showSnackWithAction(
            @NotNull view: View,
            @NotNull message: String,
            @NotNull messageAction: String,
            onclick: View.OnClickListener
        ) {
            Snackbar.make(view, message, Snackbar.LENGTH_SHORT).setAction(messageAction, onclick)
                .show()
        }
    }
}