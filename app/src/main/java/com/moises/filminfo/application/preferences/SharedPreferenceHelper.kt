package com.moises.filminfo.application.preferences

import android.content.Context
import android.content.SharedPreferences
import com.moises.filminfo.application.extensions.getAppContext
import org.jetbrains.annotations.NotNull

object SharedPreferenceHelper {

    private val prefName = "filminfo_pref"
    private val token = "PREF_KEY_TOKEN"

    private val sharedPreference: SharedPreferences =
        getAppContext().getSharedPreferences(prefName, Context.MODE_PRIVATE)

    fun setToken(@NotNull token: String?) {
        sharedPreference.edit().putString(token, token).apply()
    }

    fun getToken(): String? {
        return sharedPreference.getString(token, null)
    }



}