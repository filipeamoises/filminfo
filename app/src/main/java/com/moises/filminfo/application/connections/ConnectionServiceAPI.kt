package com.moises.filminfo.application.connections

import androidx.multidex.BuildConfig
import com.moises.filminfo.application.exception.InternetException
import com.moises.filminfo.application.helpers.NetworkHelper
import com.moises.filminfo.application.preferences.SharedPreferenceHelper
import com.moises.filminfo.application.settings.FilminfoSettings
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

object ConnectionServiceAPI {

    private var auth = false
    private var baseUrl = FilminfoSettings.baseURLAPI

    fun <T> getinstance(kllass: Class<T>, auth: Boolean = false, baseUrl: String = FilminfoSettings.baseURLAPI): T {
        ConnectionServiceAPI.auth = auth
        ConnectionServiceAPI.baseUrl = baseUrl
        return retrofit().create(kllass)
    }

    private fun retrofit(): Retrofit {

        val client = OkHttpClient.Builder()
        client.readTimeout(60, TimeUnit.SECONDS)
        client.connectTimeout(15, TimeUnit.SECONDS)


        client.addInterceptor { chain ->

            if (!NetworkHelper.isOnline()) {
                throw InternetException()
            }

            return@addInterceptor chain.proceed(chain.request())
        }


        SharedPreferenceHelper.getToken()?.let { token ->

            if (auth) {

                client.addInterceptor { chain ->

                    val original = chain.request()

                    val requestOri = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Authorization", token)
                        .method(original.method(), original.body())
                        .build()

                    return@addInterceptor chain.proceed(requestOri)
                }
            }
        }

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(interceptor)
        }

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(JacksonConverterFactory.create())
            .client(client.build())
            .build()
    }
}