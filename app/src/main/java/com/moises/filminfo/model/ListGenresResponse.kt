package com.moises.filminfo.model
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.moises.filminfo.application.bases.BaseModel

@JsonIgnoreProperties(ignoreUnknown = true)
class ListGenresResponse: BaseModel() {

    @JsonProperty("genres")
    val genres: List<Genre> = listOf()

}