package com.moises.filminfo.model
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.moises.filminfo.application.bases.BaseModel

@JsonIgnoreProperties(ignoreUnknown = true)
class Genre: BaseModel() {

    @JsonProperty("id")
    val id: Int = 0

    @JsonProperty("name")
    val name: String = ""

}