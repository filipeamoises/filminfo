package com.moises.filminfo.model

import com.moises.filminfo.application.bases.BaseModel

data class Response<out T>(val data: T?, val throwable: Throwable?, val isSuccess: Boolean = true)  : BaseModel()