package com.moises.filminfo.model

import com.moises.filminfo.application.bases.BaseModel
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class ExampleModel : BaseModel() {

    @JsonProperty("id")
    val id: Int = 0

    @JsonProperty("title")
    val title: String = ""

    @JsonProperty("body")
    val body: String = ""


}