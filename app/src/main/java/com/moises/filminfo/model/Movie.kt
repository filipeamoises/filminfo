package com.moises.filminfo.model
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.moises.filminfo.application.bases.BaseModel

@JsonIgnoreProperties(ignoreUnknown = true)
class Movie: BaseModel() {
    @JsonProperty("adult")
    val adult: Boolean = false

    @JsonProperty("backdrop_path")
    val backdropPath: String = ""

    @JsonProperty("genre_ids")
    val genreIds: List<Int> = listOf()

    @JsonProperty("genre_list")
    val genreList: ArrayList<Genre> = ArrayList()

    @JsonProperty("id")
    val id: Int = 0

    @JsonProperty("original_language")
    val originalLanguage: String = ""

    @JsonProperty("original_title")
    val originalTitle: String = ""

    @JsonProperty("overview")
    val overview: String = ""

    @JsonProperty("popularity")
    val popularity: Double = 0.0

    @JsonProperty("poster_path")
    val posterPath: String = ""

    @JsonProperty("release_date")
    val releaseDate: String = ""

    @JsonProperty("title")
    val title: String = ""

    @JsonProperty("video")
    val video: Boolean = false

    @JsonProperty("vote_average")
    val voteAverage: Double = 0.0

    @JsonProperty("vote_count")
    val voteCount: Int = 0
}