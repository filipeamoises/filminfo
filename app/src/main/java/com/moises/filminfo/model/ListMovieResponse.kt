package com.moises.filminfo.model
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.moises.filminfo.application.bases.BaseModel

@JsonIgnoreProperties(ignoreUnknown = true)
class ListMovieResponse: BaseModel() {

    @JsonProperty("results")
    val movies: List<Movie> = listOf()

    @JsonProperty("page")
    val page: Int = 0
}