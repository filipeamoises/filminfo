package com.moises.filminfo.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.moises.filminfo.MainActivity
import com.moises.filminfo.R
import com.moises.filminfo.application.bases.BaseActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().apply {
            postDelayed({
                init()
            }, 2000)
        }
    }

    private fun init() {

        Intent(this, MainActivity::class.java).apply {
            startActivityWithAnimation(this)
            finish()
        }
    }
}
