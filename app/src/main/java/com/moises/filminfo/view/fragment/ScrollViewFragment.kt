package com.moises.filminfo.view.fragment

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView
import com.moises.filminfo.R
import com.moises.filminfo.application.bases.BaseFragment
import com.moises.filminfo.application.helpers.ContextHelper
import com.moises.filminfo.model.Movie
import com.moises.filminfo.adapters.MovieViewPagerBus
import com.moises.filminfo.viewmodels.ListMoviesViewModel
import com.squareup.picasso.Picasso

class ScrollViewFragment : BaseFragment() {

    private lateinit var listMoviesViewModel: ListMoviesViewModel

    companion object {

        @JvmStatic
        fun newInstance(movie: Movie = Movie()) =
            ScrollViewFragment().apply {
                val args = Bundle()
                val fragment = ScrollViewFragment()
                arguments = args
                args.putSerializable("dataMovie", movie)
                fragment.setArguments(args)
                return fragment
            }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       return inflater.inflate(R.layout.fragment_scroll, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movie = getArguments()?.getSerializable("dataMovie") as Movie
        view.findViewById<TextView>(R.id.title).setText(movie.title)

        var summary = view.findViewById<TextView>(R.id.summary)
        summary.setText(getString(R.string.overview, movie.overview))
        summary.setMovementMethod( ScrollingMovementMethod());

        view.findViewById<TextView>(R.id.rating).setText(getString(R.string.rating, movie.voteAverage.toString()))

        view.findViewById<TextView>(R.id.release_date).setText(getString(R.string.launch, movie.releaseDate))

        var genreNames: String = ""
        movie.genreList.forEach { genre -> genreNames = genreNames + genre.name + ", " }
        view.findViewById<TextView>(R.id.genre).setText(getString(R.string.genre, genreNames.substring(0, genreNames.length - 2)))

        view.findViewById<TextView>(R.id.adult).visibility = if(movie.adult) View.VISIBLE else  View.INVISIBLE

        Picasso.get().load("https://image.tmdb.org/t/p/w500/" + movie.backdropPath).into(view.findViewById<ImageView>(R.id.iv_background))

        MovieViewPagerBus.registerScrollView(ContextHelper.getAppContext(),  view.findViewById<ObservableScrollView>(R.id.scrollView))
    }


}