package com.moises.filminfo

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.moises.filminfo.application.bases.BaseActivity
import com.moises.filminfo.databinding.ActivityMainBinding
import com.moises.filminfo.view.fragment.ScrollViewFragment
import com.moises.filminfo.viewmodels.ListMoviesViewModel

class MainActivity : BaseActivity() {

    private lateinit var databindingMain: ActivityMainBinding

    private lateinit var listMoviesViewModel: ListMoviesViewModel

    var page = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        databindingMain = DataBindingUtil.setContentView(this, R.layout.activity_main)
        listMoviesViewModel = ViewModelProviders.of(this)[ListMoviesViewModel::class.java]
        databindingMain.viewModel = listMoviesViewModel
        databindingMain.lifecycleOwner = this
        listMoviesViewModel.listGenres()
        loadNextPageMovies()
        observeMovies()
        observeHasError()
        setupRecyclerView()
    }

    private fun loadNextPageMovies() {
        page ++
        //databindingMain.swipeContainer.isRefreshing = true
        listMoviesViewModel.listPaged(page)
    }

    private fun observeMovies() {
        listMoviesViewModel.movies.observe(this, Observer {
            val movies = it ?: emptyList()

            databindingMain.hollyViewPager.setAdapter(object : FragmentPagerAdapter(supportFragmentManager) {
                override fun getItem(position: Int): Fragment {
                    return ScrollViewFragment.newInstance(movies[position])
                }


                override fun getCount(): Int {
                    return if (listMoviesViewModel.movies.value != null) listMoviesViewModel.movies.value!!.size else 0
                }

                override fun getPageTitle(position: Int): CharSequence? {
                    return listMoviesViewModel.movies.value!!.get(position).title
                }
            }, movies)
        })
    }

    private fun observeHasError() {
        listMoviesViewModel.hasError.observe(this, Observer {
            val hasError = it ?: false
            if (hasError) {
                Toast.makeText(
                    this,
                    R.string.error_list_movies,
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun setupRecyclerView() {

        databindingMain.hollyViewPager.viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                if(position > (listMoviesViewModel.movies.value?.size ?: 0) -3 && listMoviesViewModel.isLoading.value == false) {
                    loadNextPageMovies()
                }
            }
        })

    }

}

