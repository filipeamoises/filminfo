package com.moises.filminfo.viewmodels

import androidx.lifecycle.MutableLiveData
import com.moises.filminfo.application.bases.BaseViewModel
import com.moises.filminfo.application.callback.ViewModelCallback
import com.moises.filminfo.application.helpers.ToastHelper
import com.moises.filminfo.model.*
import com.moises.filminfo.services.MovieService

class ListMoviesViewModel : BaseViewModel() {

    private val movieService by lazy {
        MovieService()
    }

    val movies: MutableLiveData<List<Movie>> = MutableLiveData()

    val genres: MutableLiveData<List<Genre>> = MutableLiveData()

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()

    val hasError: MutableLiveData<Boolean> = MutableLiveData()

    fun listPaged(page: Int) {
        isLoading.postValue(true)
        if (genres.value == null || genres.value!!.isEmpty()) listGenres()

        movieService.getMoviesPaged(page, object : ViewModelCallback<ListMovieResponse> {
            override fun onSuccess(response: ListMovieResponse) {
                val completeList = ArrayList<Movie>();
                movies.value?.let { completeList.addAll(it) }
                response.movies?.let { completeList.addAll(it) }
                response.movies?.let {
                    it.forEach { movie ->
                        movie.genreIds.forEach { i ->
                            genres.value?.filter { genre -> genre.id == i }
                                ?.first()?.let { it1 -> movie.genreList.add(it1) }
                        }
                    }
                }
                movies.postValue(completeList)
                isLoading.postValue(false)
            }

            override fun onError(throwable: Throwable) {
                ToastHelper.showLongToast(throwable.localizedMessage)

            }
        })
    }

    fun listGenres() {

        movieService.getGenres(object : ViewModelCallback<ListGenresResponse> {
            override fun onSuccess(response: ListGenresResponse) {
                genres.postValue(response.genres)
            }

            override fun onError(throwable: Throwable) {
                ToastHelper.showLongToast(throwable.localizedMessage)
            }
        })
    }

}