package com.moises.filminfo.interfaces

import com.moises.filminfo.application.callback.ViewModelCallback
import com.moises.filminfo.model.ListGenresResponse
import com.moises.filminfo.model.ListMovieResponse

interface IMovieService {

    fun getMoviesPaged(page: Int, ret: ViewModelCallback<ListMovieResponse>)

    fun getGenres(viewModelCallback: ViewModelCallback<ListGenresResponse>)
}