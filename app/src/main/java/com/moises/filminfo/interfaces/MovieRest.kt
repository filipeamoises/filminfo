package com.moises.filminfo.interfaces

import com.moises.filminfo.BuildConfig
import com.moises.filminfo.application.settings.FilminfoSettings
import com.moises.filminfo.model.ExampleModel
import com.moises.filminfo.model.ListGenresResponse
import com.moises.filminfo.model.ListMovieResponse
import com.moises.filminfo.model.Movie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieRest {

    @GET("3/movie/popular")
    fun getMoviesPaged(@Query("page") page: Int, @Query("api_key") apiKey: String = FilminfoSettings.baseKEYAPI, @Query("language") language: String = "pt-BR"): Call<ListMovieResponse>

    @GET("3/genre/movie/list")
    fun getListGenres(@Query("api_key") apiKey: String = FilminfoSettings.baseKEYAPI, @Query("language") language: String = "pt-BR"): Call<ListGenresResponse>
}