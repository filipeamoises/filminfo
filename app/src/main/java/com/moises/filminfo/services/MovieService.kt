package com.moises.filminfo.services

import com.moises.filminfo.application.callback.ServiceCallback
import com.moises.filminfo.application.callback.ViewModelCallback
import com.moises.filminfo.application.connections.ConnectionServiceAPI
import com.moises.filminfo.interfaces.MovieRest
import com.moises.filminfo.interfaces.IMovieService
import com.moises.filminfo.model.ListGenresResponse
import com.moises.filminfo.model.ListMovieResponse

class MovieService : IMovieService {
    private fun getInstanceConnection(auth: Boolean = false): MovieRest {
        return ConnectionServiceAPI.getinstance(MovieRest::class.java, false)
    }

    override fun getMoviesPaged(page: Int, viewModelCallback: ViewModelCallback<ListMovieResponse>) {
        getInstanceConnection(false).getMoviesPaged(page).enqueue(ServiceCallback<ListMovieResponse>().build(
            viewModelCallback
        ))
    }
    override fun getGenres(viewModelCallback: ViewModelCallback<ListGenresResponse>) {
        getInstanceConnection(false).getListGenres().enqueue(ServiceCallback<ListGenresResponse>().build(
            viewModelCallback
        ))
    }
}
